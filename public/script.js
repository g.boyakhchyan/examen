$(document).ready(function() {
  $('.rev-slide').slick({
    infinite: true,
    speed: 300,
    prevArrow: $("#left-arrow"),
    nextArrow: $("#right-arrow"),
    fade: true,
    swipe: false, 
    draggable: false,
    slidesToShow: 1,
    adaptiveHeight: true
  });
  $('.rev-slide').on('afterChange', function(event, slick, currentSlide) {
    $('#rev-num').text('0' + (currentSlide + 1))
  });
  
  $(".first-slider").slick({
    infinite: true,
    dots: false,
    arrows: false,
    autoplay: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '10%',
    autoplaySpeed: 3000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {slidesToShow: 3}
        },
        {
            breakpoint:600, 
            settings: {slidesToShow: 2}
        }
    ]
  });

  $(".second-slider").slick({
    infinite: true,
    dots: false,
    arrows: false,
    autoplay: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '10%',
    autoplaySpeed: 4000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {slidesToShow: 3}
        },
        {
            breakpoint:600, 
            settings: {slidesToShow: 2}
        }
    ]
  });
  var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("accordion-active");
    var panel = this.lastElementChild;
    if (panel.style.maxHeight){
      panel.style.maxHeight = " ";
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
});



function saveLocalStorage() {
    localStorage.setItem("fname", $("#fname").val());
    localStorage.setItem("fnumber", $("#fnumber").val());
    localStorage.setItem("femail", $("#femail").val());
    localStorage.setItem("fmessage", $("#fmessage").val());
    localStorage.setItem("fpolicy", $("#fpolicy").prop("checked"));
}

function loadLocalStorage() {
    if (localStorage.getItem("fname") !== null)
        $("#fname").val(localStorage.getItem("fname"));
    if (localStorage.getItem("fnumber") !== null)
        $("#fnumber").val(localStorage.getItem("fnumber"));
    if (localStorage.getItem("femail") !== null)
        $("#femail").val(localStorage.getItem("femail"));
    if (localStorage.getItem("fmessage") !== null)
        $("#fmessage").val(localStorage.getItem("fmessage"));
    if (localStorage.getItem("fpolicy") !== null) {
        $("#fpolicy").prop("checked", localStorage.getItem("fpolicy") === "true");
        if ($("#fpolicy").prop("checked"))
            $("#sendButton").removeAttr("disabled");
    }
}
function clear() {
    localStorage.clear()
    $("#fname").val("");
    $("#fnumber").val("");
    $("#femail").val("");
    $("#fmessage").val("");
    $("#fpolicy").val(false);
}

$(document).ready(function() {
    loadLocalStorage();
    $("#form").submit(function(e) {
        e.preventDefault();
        let data =  $(this).serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/tZ91Xxlzzl",
            data: data,
            success: function(response){
                if(response.status == "success"){
                    alert("Спасибо за сообщение!");
                    clear();
                } else {
                    alert("Произошла ошибка: " + response.message);
                }
            }
        });
    });
    $("#fpolicy").change(function() {
        if((!this.checked)&&(grecaptcha.getResponse() === ""))
        $("#sendButton").attr("disabled", "");    
        else
        $("#sendButton").removeAttr("disabled");
    })
    $("#form").change(saveLocalStorage);
})
